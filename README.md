# Igloo Data Engineer Tech Challenge

## ✏️ Summary

Arctic Heating based in Portsmouth are looking to scale their engineering team and process a huge number of events ingested from IoT devices.

The event data is currently received in JSON format:

```
{
    "name": "smart_meter_reading",
    "timestamp": "2020-01-01T12:00:00Z",
    "reading": "200",
    "meterId": "C1234253252352"
}
```

Please build us a simple api to process these files and write them to a database of your choosing.

One endpoint of the api should ingest the data and one should expose the data via a queryable interface.

The api will need to query the database efficiently to get reads between two dates for a given meter.

## 💻 Technologies

The choice of framework to build this application is up to you. We'd suggest the use of either [Flask](https://flask.palletsprojects.com/en/1.1.x/) or [Express](https://expressjs.com/) and if possible you should demonstrate your ability to write automated tests.

## 📨 Submission

If you are familiar with Git, then please regularly commit your work as you go along so that we can see how you work. Once you're at a position to submit your work or reach the end of the allocated time limit, push up your Git repository with your code to either [Github](https://github.com), [Bitbucket](https://bitbucket.org) or similar and [send us an email](mailto:techchallenge@igloo.energy) with the link. We will then arrange a time for a call with you to discuss your solution.

**Note**: please also make sure to include a `README.md` file explaining how to get your application up and running, documenting any design choices you made and any extra information you'd like us to be aware of when reviewing your work.

## ❓Help

This is a fairly open ended task and so we'd advise that you spend no more than 3 hours of your time working on this. We're not looking for a fully fledged API but we want to get an idea of what you're capable of and how you work.

If you get stuck, have any issues getting started or have any questions throughout the task, please [send us an email](mailto:techchallenge@igloo.energy) and we'll assist you however we can.

Good luck!
